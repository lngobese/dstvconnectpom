package automationFramework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.util.concurrent.TimeUnit;

/**
 * Created by Lindokuhle.Ngobese on 2017/06/20.
 */
public class SignIn_POF {

    private static WebDriver driver;

    @FindBy(how = How.ID, using = "email" )  private static WebElement user;

    @FindBy(how = How.ID, using = "password") private static WebElement pass;

    @FindBy(how = How.XPATH, using = ".//*[@id='login']") private static WebElement Submit;

    @FindBy(how = How.XPATH, using = ".//*[@id='upload']") private static WebElement verifyProfile;

    public static void main(String[] args) throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C://chromedriver.exe");

        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("https://stagingapps.dstv.com/connect/LCC/4.1");


        PageFactory.initElements(driver, SignIn_POF.class);

        user.sendKeys("commerce01@mailinator.com");
        pass.sendKeys("123456");
        Submit.click();
        verifyProfile.isDisplayed();


        driver.close();
    }

}
