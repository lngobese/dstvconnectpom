package automationFramework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import pageObjects.*;

import java.util.concurrent.TimeUnit;

/**
 * Created by Lindokuhle.Ngobese on 2017/06/20.
 */
public class POF_TestCases {

    static WebDriver driver;

    loginPage LogIn;
    profilePage TestUpdateProfile;
    myDStvPage  myDStv;
    Newsletters newsletters;
    registerNoFlowPage regNoFlow;
    registerYesFlow regYesFlow;

    @BeforeMethod

    public void Before(){

        System.setProperty("webdriver.chrome.driver", "C://chromedriver.exe");

        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get("https://connect.dstv.com/4.1/RegisterFlow/Smartcard");

        LogIn = PageFactory.initElements(driver, loginPage.class);
        TestUpdateProfile = PageFactory.initElements(driver, profilePage.class);
        myDStv = PageFactory.initElements(driver, myDStvPage.class);
        newsletters = PageFactory.initElements(driver, Newsletters.class);
        regNoFlow = PageFactory.initElements(driver, registerNoFlowPage.class);
        regYesFlow = PageFactory.initElements(driver, registerYesFlow.class);

    }

    @Test
    public  void Test () throws InterruptedException {

        //regNoFlow.signUpActions("767259300");
        //regNoFlow.signUpActions(newOTP);

        //regYesFlow.registerActions("6102060172085");
        regYesFlow.registerActions("+23412793769", "1018332037", "Testing", "123456");
        driver.navigate().to("https://connect.dstv.com/4.1");
        TestUpdateProfile.profilePageActions();
        TestUpdateProfile.clearAllFields();
        TestUpdateProfile.updateAllFields("AutoTester", "Selenium", "PageFactory");
        myDStv.goToMyDStv.click();

        newsletters.goToNewsletters.click();
        LogIn.LogoutActions();

        //LogIn.LogInActions("commerce01@mailinator.com","123456");


    }


    @AfterMethod
    public void After(){

        driver.close();
    }

}
