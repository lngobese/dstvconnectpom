package automationFramework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

/**
 * Created by Lindokuhle.Ngobese on 2017/06/15.
 */
public class SignIn_TestCase {
    private  static WebDriver driver = null;

    public  static  void main(String[] args) throws InterruptedException {

        System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");

        driver = new ChromeDriver();
        driver.get("https://stagingapps.dstv.com/connect/LCC/4.1/");
        driver.manage().timeouts().implicitlyWait(15, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.findElement(By.id("email")).sendKeys("commerce01@mailinator.com");
        driver.findElement(By.id("password")).sendKeys("123456");
        Thread.sleep(5000);
        driver.findElement(By.id("login")).click();
        Thread.sleep(10000);
        driver.findElement(By.xpath(".//*[@id='upload-form']/div[1]/img")).isDisplayed();
        driver.close();
    }

}
