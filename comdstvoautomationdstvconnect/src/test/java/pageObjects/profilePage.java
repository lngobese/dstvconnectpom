package pageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.server.handler.FindElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

import java.security.Key;
import java.util.Date;

/**
 * Created by Lindokuhle.Ngobese on 2017/06/20.
 */
public class profilePage {
    final WebDriver driver;

    @FindBy(how = How.XPATH, using = "html/body/div[4]/div/div[1]/div/div/a[1]/div")
    public WebElement editProfileButton;

    @FindBy(how = How.ID, using = "UserName")
    public WebElement UserName;

    @FindBy(how = How.ID, using = "FirstName")
    public WebElement FirstName;

    @FindBy(how = How.ID, using = "Surname")
    public WebElement Surname;

    @FindBy(how = How.ID, using = "DateOfBirth")
    public WebElement DateOfBirth;

    @FindBy(how = How.ID, using = "save-details")
    public WebElement SaveDetails;

    @FindBy(how = How.XPATH, using = ".//*[@id='avatar-link']")
    public WebElement UploadImage;




    public profilePage(WebDriver driver){
        this.driver = driver;
    }

    public void profilePageActions(){

        editProfileButton.click();
    }

    public void clearAllFields(){
        UserName.sendKeys(Keys.CONTROL, "a", Keys.BACK_SPACE);
        FirstName.sendKeys(Keys.CONTROL, "a", Keys.BACK_SPACE);
        Surname.sendKeys(Keys.CONTROL, "a", Keys.BACK_SPACE);
        //DateOfBirth.sendKeys(Keys.CONTROL, "a", Keys.BACK_SPACE);
    }

    public void updateAllFields(String usrName, String frstName, String lastName) throws InterruptedException {



        UserName.sendKeys(usrName);
        FirstName.sendKeys(frstName);
        Surname.sendKeys(lastName);
        //DateOfBirth.findElement(By.id("DateOfBirth")).getAttribute("1990-01-02");

        DateOfBirth.sendKeys("1990-01-15");
        UploadImage.sendKeys("C://Users//lindokuhle.ngobese//Pictures//Upgrade.png");
        SaveDetails.click();

        }
}
