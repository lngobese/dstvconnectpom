package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by Lindokuhle.Ngobese on 2017/06/21.
 */
public class myDStvPage {
    final WebDriver driver;

    @FindBy(how = How.XPATH, using = "html/body/div[2]/ul/li[2]/a")
    public WebElement goToMyDStv;

    @FindBy(how = How.XPATH, using = ".//*[@id='form-1018332037']/div[1]/div/button")
    public WebElement unlink;


    public myDStvPage(WebDriver driver){
        this.driver = driver;
    }

    public void setGoToMyDStv() {
        goToMyDStv.click();
        unlink.click();

    }
}
