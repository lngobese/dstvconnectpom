package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by Lindokuhle.Ngobese on 2017/08/10.
 */
public class registerNoFlowPage {
    final WebDriver driver;

    @FindBy(how = How.XPATH, using = "html/body/div[2]/div[2]/form/div[10]/div/a/span")
    public WebElement register;

    @FindBy(how = How.XPATH, using = ".//*[@id='no']")
    public WebElement notCustomer;

    @FindBy(how = How.XPATH, using = ".//*[@id='MobileNumber']")
    public WebElement mobileNumber;

    @FindBy(how = How.XPATH, using = ".//*[@id='btnContinue']")
    public WebElement continueRegister;

    @FindBy(how = How.XPATH, using = "html/body/div[1]/div[3]/form/div[2]/div[3]/div[1]/ul")
    public WebElement passOTP;

    @FindBy(how = How.XPATH, using = ".//*[@id='btnVerifyOtp']")
    public WebElement next;

    public registerNoFlowPage(WebDriver driver){
        this.driver = driver;
    }

    public void signUpActions(String mobileNo){

        //String OTP = getOTP.opt(newOTP);

        register.click();
        notCustomer.click();
        mobileNumber.sendKeys(mobileNo);
        continueRegister.click();
        passOTP.sendKeys();

    }








}
