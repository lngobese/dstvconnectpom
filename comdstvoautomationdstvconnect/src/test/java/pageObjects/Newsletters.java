package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by Lindokuhle.Ngobese on 2017/06/21.
 */
public class Newsletters {
    final WebDriver driver;

    @FindBy(how = How.XPATH, using = "html/body/div[2]/ul/li[3]/a")
    public WebElement goToNewsletters;

    public Newsletters(WebDriver driver){

        this.driver = driver;
    }

    public void goToNewletter(){
        goToNewsletters.click();
    }

}