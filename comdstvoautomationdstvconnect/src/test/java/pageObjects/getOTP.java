package pageObjects;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import org.openqa.selenium.remote.Response;
import org.testng.Assert;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;

import static javafx.beans.binding.Bindings.equal;
import static javafx.beans.binding.Bindings.integerValueAt;
import static javafx.beans.binding.Bindings.when;

/**
 * Created by Lindokuhle.Ngobese on 2017/06/22.
 */
public class getOTP {
    private static String optAPI = "http://api-intern.dstv.com:8080/otp-gateway/v1/otp/+27767259300/history";

    public static String opt ( String cellNo){
        com.jayway.restassured.response.Response response = RestAssured.when().get(optAPI);
        Assert.assertEquals(response.asString(), 200 );

        System.out.println(response.asString());
        String newOTP = response.then().contentType(ContentType.JSON).extract().path("items[-1].otp");
        System.out.println(newOTP);
        return newOTP;
    }

}
