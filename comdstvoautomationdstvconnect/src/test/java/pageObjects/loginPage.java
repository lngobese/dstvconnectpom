package pageObjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by Lindokuhle.Ngobese on 2017/06/20.
 */
public class loginPage {

    final WebDriver driver;

    @FindBy(how = How.ID, using = "email")

    public static WebElement user;

    @FindBy(how = How.ID, using = "password")
    public static WebElement pass;

    @FindBy(how = How.XPATH, using = ".//*[@id='login']")
    public static WebElement Submit;

    @FindBy(how = How.XPATH, using = ".//*[@id='connect']/div/div[1]")
    public static WebElement connectDropdwn;

    @FindBy(how = How.XPATH, using = ".//*[@id='connect']/div/div[2]/div/div[5]/a")
    public static WebElement logout;

    public loginPage(WebDriver driver){

        this.driver = driver;

    }

    public void LogInActions(String username, String password)  {
        user.sendKeys(username);
        pass.sendKeys(password);

        Submit.click();
    }

    public void LogoutActions() throws InterruptedException {
        connectDropdwn.click();
        logout.click();

    }
}
