package pageObjects;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;

/**
 * Created by Lindokuhle.Ngobese on 2017/08/28.
 */
public class registerYesFlow {
    final WebDriver driver;

    @FindBy(how = How.XPATH, using = "html/body/div[2]/div[2]/form/div[10]/div/a/span")
    public WebElement register;

    @FindBy(how = How.XPATH, using = ".//*[@id='yes']")
    public WebElement yesFlow;

    @FindBy(how = How.XPATH, using = ".//*[@id='IdOrPassportNumber']")
    public WebElement idNumber;

    @FindBy(how = How.XPATH, using = ".//*[@id='MobileNumber']")
    public WebElement cellNum;

    @FindBy(how = How.XPATH, using = ".//*[@id='SmartcardNumber']")
    public WebElement smartcardNo;

    @FindBy(how = How.XPATH, using = ".//*[@id='nextButton']")
    public WebElement nextButton;

    @FindBy(how = How.ID, using = "FirstName")
    public WebElement firstName;

    @FindBy(how = How.ID, using = "EmailAddress")
    public WebElement emailAddr;

    @FindBy(how = How.ID, using = "Password")
    public WebElement createPassword;

    @FindBy(how = How.ID, using = "registerButton")
    public WebElement registerButton;

    public registerYesFlow(WebDriver driver){
        this.driver = driver;
    }

    public void registerActions(String cellNo, String smartcNo, String fName, String pass){

        String NewEmail;

        for (int i = 0; i < 5; i++){
            NewEmail = "automationn" + i + "@mailinator.com";

        //register.click();
        //yesFlow.click();
        //idNumber.sendKeys(idNo);
        cellNum.sendKeys(Keys.CONTROL, "a", Keys.BACK_SPACE);
        cellNum.sendKeys(cellNo);
        smartcardNo.sendKeys(smartcNo);
        nextButton.click();
        firstName.sendKeys(fName);
        emailAddr.sendKeys(NewEmail);
        createPassword.sendKeys(pass);
        registerButton.click();

        }

    }










}
